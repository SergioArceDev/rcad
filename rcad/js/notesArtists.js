
$(document).ready(function(){
$("#listTC").val($("#listTC").val().replace(/[\[\]']+/g,''));

            
var tasks = $("#listTC").val();

var myTasks = JSON.parse("[" + tasks + "]");

        // EXTRACT VALUE FOR HTML HEADER. 
        // ('Book ID', 'Book Name', 'Category' and 'Price')
        var col = [];
        for (var i = 0; i < myTasks.length; i++) {
            for (var key in myTasks[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");
        

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        var arrayID ='';
        for (var i = 0; i < myTasks.length; i++) {

            tr = table.insertRow(-1);
            
            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = myTasks[i][col[j]];
            }
            //alert(myTasks[i][col[7]]);  
             
            arrayID = arrayID+','+myTasks[i][col[7]];
            
            
        }
        
        
        
        

        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("showData");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);

$("table").addClass("table table-bordered table-hover table-striped dtable");


$('#showData').each(function(){ 
   $('table').attr('id', 'tabletasks');
});


$('#tabletasks').prepend('<thead></thead>'); // Add thead
$('#tabletasks tr:eq(0)').prependTo('#tabletasks thead'); // move first tr to thead
$('#tabletasks thead').html(function(id, html) {  // you might use callback to set html in jquery
    return html.replace(/td>/g, 'th>'); // replace td by th
                       
});   




$('#tabletasks th').eq(0).text('Requested');		
$('#tabletasks th').eq(1).text('Name');		
$('#tabletasks th').eq(2).text('Description');		
$('#tabletasks th').eq(3).text('Needed');		
$('#tabletasks th').eq(4).text('Status');		
$('#tabletasks th').eq(5).text('Assigned');
$('#tabletasks th').eq(6).text('Req');		
$('#tabletasks th').eq(7).text('Time');	

$('#tabletasks th').eq(8).hide();	
$('#tabletasks td:nth-child(8)').hide();


var newarray = arrayID.substring(1);
var arr = newarray.split(',');
for(i=0; i < arr.length; i++){
var timeloop = i+1;
var findT = "time"+timeloop;

var findN = "notes"+timeloop;
var resultID = arr[i];
 $("#tabletasks td:contains('" + findT + "')").html('<a id="' + resultID + '" class="btn btn-success time" onClick="idAlert(this.id)"><span class="glyphicon glyphicon-play"></span></a>')
 $("#tabletasks td:contains('" + findN + "')").html('<a id="' + resultID + '" data-toggle="modal" data-target="#modalNotes" class="btn btn-warning notes"><span class="glyphicon glyphicon-list-alt"></span></a>')
}
$('.dtable').dataTable();
$("table").removeClass("dataTable");

});

function idAlert(id)
{
    //alert(id);
}