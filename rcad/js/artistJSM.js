
$(document).ready(function(){

var doSomethingOnceValueIsPresent = function () {//Begins doSomethingOnceValueIsPresent
$("#tabletasks").addClass("table table-bordered table-hover table-striped");

if ($('#listTC').val() != ''){
    
checkVariable();



var idMaxF = $("#idmax").val();    
if(idMaxF == ''){ $( "#stopTime" ).hide();}
else{$( "#stopTime" ).show();}




$(".postTime").click(function(){
    var iduser = $("#userId").val();
    var internalIdMax = $("#internalIdMax").val();
    var str = $(this).attr('id');
    var res = str.split("&");
    var itemId = "";
    var productTypeId = "";
    for (var i = 0; i < res.length; i++) {
        itemId = res[0];
        productTypeId = res[1];
    }

if(idMaxF == ''){
$.post('timeProcessOne.php', {'idu':iduser, 'itemid':itemId, 'productTId': productTypeId, 'internalMaxId': internalIdMax}, 
function (response) {
alertify.set('notifier', 'position', 'top-right');
alertify.notify('Started a task', 'custom', 3, function(){setTimeout("location.reload();", 1200);});
});}else{
$.post('time.php', {'idu':iduser, 'itemid':itemId, 'productTId': productTypeId, 'internalMaxId': internalIdMax}, 
function (response) {
alertify.set('notifier', 'position', 'top-right');
alertify.notify('Started a task', 'custom', 3, function(){setTimeout("location.reload();", 1200);});
});
}
});

$(".updateItemF").click(function(){
    var statusId = $('#statusVal').val();
    var reqById = $('#reqByVal').val();
    var itemId = $(this).attr('id');
if(statusId == '' && reqById == ''){
    $('#modalErrorUpdate').modal();
}else{
 $.post('updateItem.php', {'itemId':itemId, 'status':statusId, 'reqby': reqById}, 
function (response) {
alertify.set('notifier', 'position', 'top-right');
alertify.notify('Status Updated', 'custom', 3, function(){setTimeout("location.reload();", 1000);});
});   
}
});


$(".notes").click(function(){
$("#itemIdNote").val('');    
$("#itemIdNote").val($(this).attr('id'));
}); 

$("#modalNotes").on("hidden.bs.modal", function () {$("textarea#noteText" ).val('');});

}else{
    setTimeout(function(){
    doSomethingOnceValueIsPresent()
    }, 2000);
}
    
};//Ends doSomethingOnceValueIsPresent
doSomethingOnceValueIsPresent();

//Number 2        
var doSomethingOnceValueIsPresentTwo = function () {//Begins doSomethingOnceValueIsPresent2

if ($('#userRBT').val() != ''){
    var usersRB = $("#userRBT").val();
    var usersRBList = JSON.parse('[' + usersRB + ']');        		
    $.each(usersRBList, function (key, value) {
    $(".dropDownReqBy").append($('<option></option>').val(value.ID).html(value.NAME));
    });
    $('.dropDownReqBy').click(function () {$('.dropDownReqBy').prop('selectedIndex',0);});
        
    //Here is the Status
    var t1 = $("#itemSL").val();
    var t2 = t1.substring(1);
    var t3 = JSON.parse("[" + t2 + "]");
for (var i = 0; i < t3.length; i++) {
    //Here is the Item Status by Id Begins
    var itemstatusV = $("#itemStatusL").val();
    var itemsList = JSON.parse('[' + itemstatusV + ']');        		
$.each(itemsList, function (key, value) {$("#"+t3[i]+"ItemS").append($('<option></option>').val(value.ID).html(value.NAME));});
//Here is the Item Status by Id Ends
    var usedNames = {};
$("select#"+t3[i]+"ItemS > option").each(function () {
if(usedNames[this.text]) {$(this).remove();} else {usedNames[this.text] = this.value;}
});
}
$('.itemLStatus').change(function () {
$('#statusVal').val('');
$('#statusVal').val($(this).val());
});

$('.dropDownReqBy').change(function () {
$('#reqByVal').val('');
$('#reqByVal').val($(this).val());
});

$('.itemLStatus').click(function () {$('.itemLStatus').prop('selectedIndex',0);});

//Begins DataTable Principal
// Setup - add a text input to each footer cell

    var table = $('#tabletasks').dataTable( {
     columnDefs: [{ type: 'date-eu', targets: 0 }, { targets: 7, orderable: false}],"iDisplayLength": 100, "order": [[ 0, "desc" ]]
    });
    table.api().columns().every( function () {
    var that = this;
    $( 'select', this.header() ).on( 'keyup change', function () {
    if ( that.search() !== this.value ) {
    that.columns(11).search( this.value ).draw();
     if(this.value === " "){
    $('#statusListS option').prop('selected', function() {
    return this.defaultSelected;
    });
    }}
    });
    });


$('.dtable').dataTable();
$("table").removeClass("dataTable");  

//Ends DataTable Principal
var maxStatusv = $("#maxStatus").val();
$("#statusListS").val(maxStatusv).trigger('change');

}else{
        setTimeout(function(){
            doSomethingOnceValueIsPresentTwo()
            }, 2000);
}
    
};//Ends doSomethingOnceValueIsPresent2

doSomethingOnceValueIsPresentTwo();


});



function checkVariable(){//Begin checkVariable function
//Begin the big process
    var idmaxVar = $('#idmax').val();
    $("#listTC").val($("#listTC").val().replace(/[\[\]']+/g,''));
    var text = $("#listTC").val();
    //Here we find the web comments <!--(.*?)\-->
    var matches = text.match(/\<!--(.*?)\-->/g);
    var newText = ['', '', '', ''];
$.each(matches, function(index, match) {text = text.replace(match, newText[index]);});
    $("#listTC").val(text);
    //End the final contruction       
    var tasks = $("#listTC").val();
    var myTasks = JSON.parse("[" + tasks + "]");
    // EXTRACT VALUE FOR HTML HEADER. 
    // ('Book ID', 'Book Name', 'Category' and 'Price')
    var col = [];
for (var i = 0; i < myTasks.length; i++) {
    for (var key in myTasks[i]) {
        if (col.indexOf(key) === -1) {col.push(key);}
    }
}
    // CREATE DYNAMIC TABLE.
    var table = document.createElement("table");
    //var table = document.getElementById("tabletasks");
    // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
    var tr = table.insertRow(-1);                   // TABLE ROW.
for (var i = 0; i < col.length; i++) {
    var th = document.createElement("th");      // TABLE HEADER.
    th.innerHTML = col[i];
    tr.appendChild(th);
}
    // ADD JSON DATA TO THE TABLE AS ROWS.
    var arrayID ='';

for (var i = 0; i < myTasks.length; i++) {
    tr = table.insertRow(-1);
    for (var j = 0; j < col.length; j++) {
    var tabCell = tr.insertCell(-1);
    tabCell.innerHTML = myTasks[i][col[j]];
    }
    arrayID = arrayID+','+myTasks[i][col[10]];
}
    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
    var divContainer = document.getElementById("showData");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);
$("table").addClass("table table-bordered table-hover table-striped");
$('#showData').each(function(){$('table').attr('id', 'tabletasks');});
$('#tabletasks').prepend('<thead></thead>'); // Add thead
$('#tabletasks tr:first').prependTo('#tabletasks thead'); // move first tr to thead
$('#tabletasks thead').html(function(id, html) {  // you might use callback to set html in jquery
    return html.replace(/td>/g, 'th>'); // replace td by th
});    

$('#tabletasks th').eq(0).text('Needed');		
$('#tabletasks th').eq(1).text('Name');		
$('#tabletasks th').eq(2).text('Time');		
$('#tabletasks th').eq(3).text('Notes');		
$('#tabletasks th').eq(4).text('Description');		
//$('#tabletasks th').eq(5).text('Status');

//I did Changes Begin

//$('#tabletasks th').eq(5).text('Division');
$('#tabletasks th').eq(8).text('Required');
$('#tabletasks th').eq(9).text('Action');


//$('#tabletasks th').eq(7).hide();	
$('#tabletasks th').eq(10).hide();	
$('#tabletasks th').eq(11).hide();	
//$('#tabletasks td:nth-child(8)').hide();
$('#tabletasks td:nth-child(11)').hide();
$('#tabletasks td:nth-child(12)').hide();

$('#tabletasks td:nth-child(9)').html('<select class="dropDownReqBy"><option selected disabled>Required By</option></select>');
//$('#tabletasks td:nth-child(6)').html('<select class="itemLStatus"></select>');
//$('#tabletasks td:nth-child(7)').html('<h1>hello</h1>');

$('#tabletasks thead th').eq(7).each( function () {
    var title = $(this).text();
    //$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    $(this).html( '<select id="statusListS" style="color:black;" onchange="insertFStatus()"><option selected="selected" value="" disabled>Select an Status</option><option value=" ">None</option><option>Approved</option><option>Approved With Changes</option><option>Canceled</option><option>Completed</option><option>In Licensing</option><option>In Progress</option><option>Not Started</option><option>On Hold</option><option>PreProduction Sampling</option><option>Re-Submission Needed</option><option>Revisions Needed</option><option>Subcomponent / Kit</option><option>Submission Needed</option></select>' );
});
//Estimated Time Change begin
$('#tabletasks td:nth-child(7)').each( function () {
    var valueEst = $(this).text();
    if (valueEst == ''){
        $(this).text("0");
    }
});

//Estimated Time Change end

    
//I did Changes End    
    var newarray = arrayID.substring(1);
    var arr = newarray.split(',');
for(i=0; i < arr.length; i++){//Begin the For
    var timeloop = i+1;
    var findT = "time"+timeloop;
    var findN = "notes"+timeloop;
    var resultID = arr[i].substring(0, arr[i].indexOf('.'));

$(".notesAc").click(function(){
    
    var show_id = $(this).attr('id');
    
    $.post('../php/notes.php', {postidTask:show_id}, 
    function(data){ 
    $("#notesByTask").val(data);
        if ($.trim($("#notesByTask").val())) {checkNotes();}});
        
    if (!$.trim($("#notesByTask").val())) {console.log("test del vacio");} 
});
    
    var noti= arr[i].split(/\./)[1];
    var idSingle= arr[i].split(/\&/)[0];
    $("#itemSL").val($("#itemSL").val()+','+idSingle);
    
if (idmaxVar === idSingle){
    $( "."+idmaxVar+"" ).removeClass( "postTime btn-success" ).addClass( "btn-danger" );
    $( "."+idmaxVar+"" ).find('span').removeClass( "glyphicon-play" ).addClass( "glyphicon-stop" );
    $( "."+idmaxVar+"" ).attr('disabled', 'disabled');
}
 
if(noti == 0){
    $( "."+idSingle+"NoteClass" ).removeClass( "postTime btn-warning" ).addClass( "btn-danger" );
}   
}//End the For

//Ends of the big process

//Order By Date Begin
//Date Order USA Begins
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
"date-eu-pre": function ( date ) {
    date = date.replace(" ", "");
    if ( ! date ) {return 0;}
    var year;
    var eu_date = date.split(/[\.\-\/]/);
    
    /*year (optional)*/
    if ( eu_date[2] ) {
        year = eu_date[2];
    }else {
        year = 0;
    }
    /*month*/
    var month = eu_date[1];
    if ( month.length == 1 ) {
        month = 0+month;
    }
    /*day*/
    var day = eu_date[0];
    if ( day.length == 1 ) {
        day = 0+day;
    }
    return (year + day + month) * 1;
},
  
   "date-eu-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "date-eu-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});

//Date Order USA Ends

//Order By Date Ends

}//Ends checkVariable function
    
    
function checkNotes(){//Begin checkNotes function
$("#notesByTask").val($("#notesByTask").val().replace(/[\[\]']+/g,''));
    var tasks = $("#notesByTask").val();
    var myTasks = JSON.parse("[" + tasks + "]");
    // EXTRACT VALUE FOR HTML HEADER. 
    // ('Book ID', 'Book Name', 'Category' and 'Price')
    var col = [];
for (var i = 0; i < myTasks.length; i++) {
    for (var key in myTasks[i]) {
        if (col.indexOf(key) === -1) {col.push(key);}
    }
}
    // CREATE DYNAMIC TABLE.
    var table = document.createElement("table");
    // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
    var tr = table.insertRow(-1);// TABLE ROW.

for (var i = 0; i < col.length; i++) {
    var th = document.createElement("th");//TABLE HEADER.
    th.innerHTML = col[i];
    tr.appendChild(th);
}
    // ADD JSON DATA TO THE TABLE AS ROWS.
    var arrayID ='';
for (var i = 0; i < myTasks.length; i++) {
    tr = table.insertRow(-1);
    for (var j = 0; j < col.length; j++) {
    var tabCell = tr.insertCell(-1);
    tabCell.innerHTML = myTasks[i][col[j]];
}
    arrayID = arrayID+','+myTasks[i][col[6]];
}
    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
    var divContainer = document.getElementById("notesDiv");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);

$('div#notesDiv').find('table').addClass('tablenote table table-bordered table-hover table-striped dtable');
$('.tablenote').prepend('<thead></thead>'); // Add thead
$('.tablenote tr:eq(0)').prependTo('.tablenote thead'); // move first tr to thead
$('.tablenote thead').html(function(id, html) {  // you might use callback to set html in jquery
    return html.replace(/td>/g, 'th>'); // replace td by th
});   

$(".notesAc").click(function(){
    var show_id = $(this).attr('id');
    $.post('../php/notes.php', {postidTask:show_id}, 
    function(data){ 
    $("#testarray").html(data);
    $("#notesByTask").val(data);
    });
});

$('.dtable').dataTable();
$("table").removeClass("dataTable");

}//Ends checkNotes function