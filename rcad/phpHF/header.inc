<?php
session_start();
$usernameV = $_SESSION['username'];
$id = $_SESSION['userid'];
$email = $_SESSION['emailUser'];


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RCAD - Rico Industries Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    
    <link href="../css/jquery.dataTables.min.css" rel="stylesheet">
    
    <!-- CSS -->
    <link rel="stylesheet" href="../css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="../css/default.min.css"/>
    
    <!-- Stye -->
    <link rel="stylesheet" href="../css/styleHeader.css"/>

</head>

<body>
<!-- <div id="wrapper"> -->

    <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="artist.php">RCAD - Rico Commitment To The Art Department</a>
                
                
            </div>
            
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <?php include "../phpHF/inputs.inc" ?>
                  <li><a href="imageR.php" > Image Request </a><span class="circleImg"><p id="imgrnt" ></p></span></li>
                  <li><a href="artist.php" > Dashboard </a></li>
                  <li><a href="#" ><i class="fa fa-user"></i> <?php echo $usernameV;?> </a></li>
                  <li><a id="logout" href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
            </ul>
       <!-- -->
        </nav>

        