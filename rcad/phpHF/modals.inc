
  
<!-- Modal Notes -->
  <div class="modal fade" id="modalNotes" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Task Notes</h4>
        </div>
        <div class="modal-body" >
              <h4 style="color:#F8D650;">Create a Note</h4>
              <input class="form-control" name="itemIdNote" id="itemIdNote" type="hidden">
    <textarea id="noteText" rows="3" style="width:100%"></textarea>
    <input class="btn btn-large btn-success pull-right insertNote" type="submit" id="insertNote" name="insertNote" value="Insert" />
  
        <textarea rows="20" cols="100" name="notesByTask" id="notesByTask" style="display: none;"></textarea>
        </br></br></br></br>
          <div id="notesDiv" >
            
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
    <div class="modal fade" id="modalErrorUpdate" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" style="color:red;">Update Error</h1>
        </div>
        <div class="modal-body" >
              <h4 style="color:black;">You cannot update empty values</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
<div class="modal fade" id="modalErrorNote" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" style="color:red;">Note Error</h1>
        </div>
        <div class="modal-body" >
              <h4 style="color:black;">You cannot insert an empty Note</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  

<div class="modal fade" id="modalErrorLogin" role="dialog" style=" z-index: 2000;" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" style="color:red;">Login Failure</h1>
        </div>
        <div class="modal-body" >
              <h4 style="color:black;">The Email or Password is incorrect, please try again.</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
<div class="modal fade" id="addBrochure" role="dialog" style=" z-index: 2000;" >
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title">New Image Request</h1>
        </div>
        <div class="modal-body" >
          
          
          <div class="input-group">
  <span class="input-group-addon" id="sizing-addon2">Date Needed</span>
  <input id="date" type="date" class="form-control">
</div>


          <div class="input_fields_wrap" style="padding-top:15px;">
          <button class="add_field_button btn btn-success" >Add More Requests</button>
          <div style="padding-top:15px;"><input type="text" class="form-control" placeholder="Tasks" name="mytext[]"></div>
          </div>  
          <div style="padding-top:15px;">
            <button class="btn btn-warning" id="addCommentsB">Add a Comment</button>
            </div>  
            
        <div style="padding-top:15px; display: none;" id="commentBrochure">
        <textarea style="resize: none;" rows="5" cols="120" name="" id="" placeholder="Type your comment for this Brochure Request"></textarea>
        </div>

        <div style="padding-top:15px;">
        <select class="dropDownReqBy form-control" ><option selected disabled>Required By</option></select>
        </div>
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Create</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
