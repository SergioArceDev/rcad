<?php
   $value = $_POST['user'];
   $uid = $_POST['id'];
   $emailU = $_POST['email'];
   session_start();
   $_SESSION['username'] = $value;
   $_SESSION['userid'] = $uid;
   $_SESSION['emailUser'] = $emailU;
?>

<html>
    <head>
        <title>RCAD</title>
  <meta charset="UTF-8">      
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel=Stylesheet href="css/styleMenu.css" >
        
        
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/javascriptMenu.js"></script>
        <script type="text/javascript">
     
        
$(document).ready(function(){



	$("#login").click(function(){
        var email = $('#email').val();
        var pass = $('#password').val();
        if(email === '' || pass ===''){
	$("#errormsg").text("You cannot have empty fields. Please put your Email and Password and try again.");
	$('#textVali').show();
	setTimeout(function(){
    	$('#textVali').hide();
    }, 7000);
    $('#email').val('');
    $('#password').val('');
    
        }else{
        	console.log("values: "+email+" "+pass);
        $.get('/php/NetSuiteVali.php',  {'postemail':email, 'postpass':pass}, function (response) {
var values = response;
console.log(values);
$("#login" ).hide();
$("#loader" ).show();
if(values === 'True'){
	$.post('php/url.php', {'postemail':email}, 
        function(data){ 
        	//$("#result").html(data);
        
        	if(data === 'error'){ 
        		alert("There is an error");
        		$("#email").val('');
        		//$("#listT").val('');
        		$("#loader" ).hide();
        		$("#login" ).show();
				
        	}else{
        		
        		var values = data;
        		//alert("Test:"+values);
				var arr = values.split('·');
				// arr[0] -> Email
				// arr[1] -> ID
				// arr[2] -> Role
				// arr[3] -> Array
				// arr[4] -> Name of the User
				//var array = arr[4];
				//$("#listT").val(array);
				var name = arr[3];
				var id = arr[1];
				
        		console.log(arr[2]);
						//alert(email);
    			    	$("#loginForm" ).submit();
    			    	
    			    	$.post('index.php', {'user' : name, 'id' : id, 'email' : email}, function (response) {
    				//alert(response);
					});
					
        	}
		});//End of the Post Fucntion
}else{
	//alert("invalid user name");
	$("#loader" ).hide();
	$("#login" ).show();
	//$('#modalErrorLogin').modal();
	$('#email').val('');
    $('#password').val('');
	$("#errormsg").text("Check the Email and Password and try again.");
	$('#textVali').show();
	setTimeout(function(){
    	$('#textVali').hide();
    }, 7000);
	
}//End of the else Get

});//End of the Get

}//End of the Else empty fields condition
});//End of the on click 


    
});

$(function(){
  //press enter on text area..
 $('#email, #password').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('#login').click();
    return false;  
  }
});


  
});
</script>

<style type="text/css">
	/*Loader Begin*/
.loader {

  border: 16px solid black;
  border-radius: 50%;
  border-top: 16px solid #E41938;
  width: 70px;
  height: 70px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/*Loader Ends*/
</style>
    </head>

    <body> 
<div class="container" style=" z-index: 999;background-color:;position: relative; " >
    <div class="row vertical-offset-100" style="background-color:;">
    	<div class="col-md-4 col-md-offset-4">
    	    <br><br><br>
    	    <img src='img/clockWhite.png' width=300; style="background-color:; display:block;margin:auto;"><br>
    		<div class="panel panel-default">
			  	<div class="panel-heading" >
			  	    <h3 class="panel-title">Please Login</h3>
			 	</div>
			  	<div class="panel-body"> 
			    	<form accept-charset="UTF-8" role="form" id="loginForm" action="php/artist.php" method="POST">
                    <fieldset>
			    	  	<div class="form-group">
			    	  		<!--<textarea rows="20" cols="100" name="listT" id="listT" ></textarea>-->
			    		    <input class="form-control" placeholder="NetSuite E-mail" name="email" id="email" type="text">
			    		</div>
			    		<div class="form-group">
			    	  		<!--<textarea rows="20" cols="100" name="listT" id="listT" ></textarea>-->
			    		    <input class="form-control" placeholder="NetSuite Password" name="password" id="password" type="password">
			    		</div>
			    		
			    		<div class="checkbox">
			    	    	<label>
			    	    		<input name="remember" type="checkbox" value="Remember Me"> Remember Me
			    	    	</label>
			    	    </div>
			    		
			    		<input class="btn btn-lg btn-success btn-block" type="button" value="Login" id="login" style="background-color:#E41938;border-color:#E41938;">
			    	<div style="display: none; margin: 0 auto;" class="loader" id="loader"></div>	
			    		
			    	</fieldset>
			      	</form>
			      	
			      	
			      	<div id="textVali" style="display: none;">
						<p style="color:red;">  Login Failed. <span id="errormsg" style="color:black;"></span></p>
					</div>
					

			      	<div id="result"></div>
	    	    </div>
			</div>
		</div>
	</div>
</div>

<div id="particles-js"></div>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js"></script>-->
    </body>
</html>
